/**Bài 1 : tính lương:
 * -input : tiền lương 1 ngày 100000 , number
 *          số ngày làm , number
 * 
 * - các bước thực hiện :
 *  + đặt biến
 *  + nhập số ngày làm
 *  + tính : số ngày làm * lương một ngày
 * 
 * 
 * 
 * - output: tổng tiền lương , number
 *      
 */


 function tinhtongluong() {
var songaycong=0;
    songaycong=document.getElementById('songaycong').value*1;
var tongluong=0;
    tongluong = songaycong * 100000;
        document.getElementById('tongluong').innerHTML=tongluong.toLocaleString();
 console.log(songaycong)
 console.log(tongluong)

 }

 
/**Bài 2: Tính giá trị trung bình
 * 
 * - input : - 5 sô thực , number
 * 
 * - progress:
 * b1 : gọi biến
 * b2 : nhập 5 số thực , number
 * b3 : tinh tổng 5 số,number
 * b4 : tính gí trị trung bình, number
 * 
 * -output :
 * kết quả trung bình, number
 *
 */
function giatritrungbinh(){
    var number1=0;
    var number2=0;
    var number3=0;
    var number4=0;
    var number5=0;
        number1=document.getElementById('number1').value*1;
        number2=document.getElementById('number2').value*1;
        number3=document.getElementById('number3').value*1;
        number4=document.getElementById('number4').value*1;
        number5=document.getElementById('number5').value*1;
    var giatritb=0;
       giatritb= (number1 + number2 + number3 + number4 + number5)/5;
  document.getElementById('giatritb').innerHTML=giatritb
     console.log(number1)
     console.log(number2)
     console.log(number3)
     console.log(number4)
     console.log(number5)
     console.log(giatritb)
}

/**Bài 3 : tính tiền:
 * -input : 1USD = 23.500 , number
 *          số usd , number
 * 
 * - các bước thực hiện :
 *  + đặt biến
 *  + nhập số usd
 *  + tính : số usd * 23500
 * 
 * 
 * 
 * - output: tính tiền , number
 *      
 */


 function tinhtien() {
    var sousd=0;
        sousd=document.getElementById('sousd').value*1;
    var tinhtien=0;
        tinhtien = sousd * 23500;
            document.getElementById('tinhtien').innerHTML=tinhtien.toLocaleString();
     console.log(sousd)
     console.log(tinhtien)    
     }

 /**Bài 4 : tính chu vi, diện tích hình chữ nhật
 * -input : chiều dài , number
 *          chiều rộng , number
 * 
 * - các bước thực hiện :
 *  + đặt biến
 *  + nhập số chiều dài, chiều rộng ,number
 *  + tính diện tích : chiều dài * chiều rộng
 *  + tính chu vi :(chiều dài + chiều rộng) *2
 * 
 * 
 * 
 * - output: diện tích , number
 *           chu vi, number
 *      
 */


  function dientich() {
    var chieudai=0;
        chieudai=document.getElementById('chieudai').value;
     var chieurong=0;
        chieurong=document.getElementById('chieurong').value;

    var tinhdientich=0;
        tinhdientich = chieudai * chieurong;
            document.getElementById('tinhdientich').innerHTML=tinhdientich.toLocaleString();
      console.log(chieudai)
      console.log(chieurong)
      console.log(tinhdientich)
} 


     function chuvi() {
        var chieudai=0;
            chieudai=document.getElementById('chieudai').value*1;
         var chieurong=0;
            chieurong=document.getElementById('chieurong').value*1;
    
        var tinhchuvi=0;
            tinhchuvi = (chieudai + chieurong)*2;
                document.getElementById('tinhchuvi').innerHTML=tinhchuvi.toLocaleString();
         console.log(chieudai)
         console.log(chieurong)
         console.log(tinhchuvi)
         }    
/**Bài 5 : tính tổng 2 kí số
 * -input : cho một số 2 chữ số, number
 * 
 * - các bước thực hiện :
 *  + đặt biến, number
 *  + nhập số , number
 *  + tính tổng kí số
 * 
 * 
 * 
 * - output: tổng kí số , number
 *      
 */


 function tongkiso() {
    var so=0;
        so=document.getElementById('so').value*1;
    var hangchuc=Math.floor(so / 10)
    var hangdonvi= so % 10
    var tongkiso=0;
        tongkiso = hangchuc + hangdonvi
            document.getElementById('tongkiso').innerHTML=tongkiso;
    
            console.log(so)
            console.log(hangchuc)
            console.log(hangdonvi)
            console.log(tongkiso)

     }
    

     